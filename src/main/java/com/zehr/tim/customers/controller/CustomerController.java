package com.zehr.tim.customers.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zehr.tim.customers.model.Customer;
import com.zehr.tim.customers.service.CustomerService;

@RestController
public class CustomerController {
	
	@Resource(name="HardCodedCustomerService")
	private CustomerService service;
	
	@Value("${spring.datasource.url}") 
	public String url;
	
	@GetMapping("customers/{id}")
	public Customer getCustomer(@PathVariable("id") Integer customerId) {
		System.out.println("using db2 url: " + url);
		return service.getCustomer(customerId);
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void addCustomer() {
		service.addCustomer();
	}
}