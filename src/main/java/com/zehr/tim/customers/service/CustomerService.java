package com.zehr.tim.customers.service;

import com.zehr.tim.customers.model.Customer;

public interface CustomerService {

	Customer getCustomer(Integer id);

	void addCustomer();

}
