//package com.zehr.tim.customers.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Service;
//
//import com.zehr.tim.customers.model.Customer;
//
//@Service
//public class DB2CustomerService implements CustomerService {
//	@Autowired
//	JdbcTemplate jdbcTemplate;
//
//	@Override
//	public Customer getCustomer(Integer id) {
//		return jdbcTemplate.query("select * from cust where cust_id=" + id, new Object[] { }, 
//				(rs, rowNum) -> new Customer(rs.getInt("cust_id"), rs.getString("first"), 
//						rs.getString("last"), rs.getInt("age"))).get(0);
//
//	}
//
//	@Override
//	public void addCustomer() {
//		jdbcTemplate.update("insert into cust(last,first,age) values('Zehr','Tim',23)");
//		jdbcTemplate.update("insert into cust(last,first,age) values('Smitts','Jimmy',35)");
//		jdbcTemplate.update("insert into cust(last,first,age) values('San Diego','Carmen',45)");
//
//	}
//
//}
