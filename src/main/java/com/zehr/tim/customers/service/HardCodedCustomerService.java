package com.zehr.tim.customers.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.zehr.tim.customers.model.Customer;

@Service(value = "HardCodedCustomerService")
public class HardCodedCustomerService implements CustomerService {
	private final static Map<Integer, Customer> CUSTOMERS = new HashMap<>();
	
	static {
		CUSTOMERS.put(1, new Customer(1, "Tim", "Zehr", 21));
		CUSTOMERS.put(2, new Customer(2, "John", "Smith", 34));
		CUSTOMERS.put(3, new Customer(3, "Julie", "Dozier", 43));
		CUSTOMERS.put(4, new Customer(4, "Jane", "Doe", 29));
	}

	@Override
	public Customer getCustomer(Integer id) {
		return CUSTOMERS.get(id);
	}

	@Override
	public void addCustomer() {
		// TODO Auto-generated method stub
		
	}

}
